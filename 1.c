/*
 * Advent of Code - Day 1
 * C89 (POSIX) by CaptainHayashi
 *
 * By default, this compiles as Part 1.
 * To compile as Part 2, define -DPART2.
 */

#include <unistd.h>
#include <fcntl.h>
#include <malloc.h>

void prnum(unsigned int n);

#ifndef PART2

/*
 * Part 1
 *
 * This is really easy: if a digit matches the next one, then there
 * exists a digit that matches the previous one.  For every possible
 * match other than the wraparound, we can just look at the previous
 * digit.
 *
 * We do this with a buffer that keeps swapping indices between last
 * and current digit.  To handle the wraparound, we make sure we store
 * the first digit in a special index that we don't overwrite later,
 * and then compare this digit to the one we last wrote.
 */

unsigned int calc1(int fd);

/* calc1 calculates Part 1. */
unsigned int
calc1(int fd)
{
	char c;
	unsigned char d[3];
	unsigned int i, l;
	unsigned int res;

	d[0] = d[1] = d[2] = 0;
	i = 0;
	l = 1; /* so that the first comparison doesn't work */

	res = 0;
	while (read(fd, &c, 1)) {
		/* assuming ASCII */
		if (c < '0' || c > '9') continue;
		d[i] = (unsigned char) (c - '0');

		/* for simplicity, instead of comparing digit i to i+1,
		   we compare i to i-1, set up the 0 comparison so it
		   is a no-op, and do wraparound separately */
		res += d[i] == d[l] ? d[l] : 0;
		l = i;
		i = i == 1 ? 2 : 1;
	}

	res += d[l] == d[0] ? d[0] : 0;  /* wraparound */
	return res;
}

#endif /* PART2 not defined */

#ifdef PART2

/*
 * Part 2
 *
 * The approach here is to store, for each digit 1 to 9, the sorted
 * positions in which each digit appears.  (We can ignore 0, since it
 * will never add anything to the digit sum.)  The sorted positions
 * form 9 doubly linked lists.
 *
 * Then, we walk through each list in one direction and, for each
 * position, walk backwards to see if there was a position that is
 * halfway around the input without circling back around.  If so, we
 * add twice that digit---since the list is 2n long, if a digit
 * matches by not wrapping around, its match wraps around and matches
 * the original, and vice versa.
 */

struct posnod;

int             posnod_add(struct posnod **curnodp, unsigned int pos);
void            posnods_free(struct posnod **nods);
unsigned int    matches(struct posnod *nod, unsigned int dindex, unsigned int ndigits);
struct posnod **drain(int fd, unsigned int *out_ndigits);
unsigned int    sum(unsigned int ndigits, struct posnod **nods);
unsigned int    calc2(int fd);

#define NUM_POSNODS 9 /* 1 to 9 */

/* struct posnod is a doubly linked list node containing a digit
   position. */
struct posnod {
	struct posnod *next;
	struct posnod *prev;  
	unsigned int pos;
};

/* posnod_add inserts a node onto the front of a posnod list. */
int
posnod_add(struct posnod **curnodp, unsigned int pos)
{
	struct posnod *new;
  
	if (curnodp == NULL) goto fail;

	new = calloc(1, sizeof(struct posnod));
	if (new == NULL) goto fail;

	new->pos = pos;

	if (*curnodp != NULL) {
		new->prev = *curnodp;
		(*curnodp)->next = new;
	}
	
	*curnodp = new;
	return 1;
fail:
	write(2, "add failed\n", 11);
	return 0;
}

/* posnods_free frees a set of NUM_POSNODS posnods. */
void
posnods_free(struct posnod **nods)
{
	unsigned int i;

	if (nods == NULL) return;
	
	for (i = 0; i < NUM_POSNODS; i++) {
		struct posnod *nod;

		nod = nods[i];
		while (nod != NULL) {
			struct posnod *tmp;

			tmp = nod->prev;
			free(nod);
			nod = tmp;
		}
	}
	free(nods);
}		

/* drain reads fd to completion and stores the number of digits in
   out_ndigits.  It also stores, in a set of NUM_POSNODS posnods, the
   positions for each digit 1 to 9. */
struct posnod **
drain(int fd, unsigned int *out_ndigits)
{
	struct posnod **posnods;
	char c;
	unsigned char d;
	unsigned int pos;
	
	posnods = calloc(NUM_POSNODS, sizeof(struct posnod *));
	if (posnods == NULL) return NULL;

	pos = 0;
	while (read(fd, &c, 1)) {
		/* assuming ASCII */
		if (c < '0' || c > '9') continue;

		/* The off-by-one doesn't matter here, since we're
		   working with offsets between positions anyway.  In
		   fact, incrementing pos like this means that it
		   finishes up as the total number of digits. */
		pos++;
		
		/* we can ignore '0' as it never adds to the digit sum */
		if (c == '0') continue;
		
		d = (unsigned char) (c - '1');
		if (!posnod_add(posnods + d, pos)) return NULL;
	}

	*out_ndigits = pos;
	return posnods;
}

/* matches takes a posnod, digit index (1-9 minus 1), and number of
   total digits.  If there exists another posnod (ndigits/2) places
   higher up from posnod, it returns the amount to be added to the
   digit sum.  If not, it returns 0. */
unsigned int
matches(struct posnod *nod, unsigned int dindex, unsigned int ndigits)
{
	unsigned int cpos, opos, score;

	/* The offset is always halfway around an even number, so each
	   match will actually add to the digit score twice: once from
	   position X to position Y, then once again from position Y
	   to position X. */
	score = (dindex + 1) * 2;
	
	cpos = nod->pos;
	opos = cpos + (ndigits / 2);
	if (opos > ndigits) return 0;

	/* Don't keep going if we overshoot the target position */
	while (nod != NULL && nod->pos <= opos) {
		if (nod->pos == opos) return score;
		nod = nod->next;
	}
	
	return 0;
}

/* sum calculates the sum of all digits that, according to posnod,
   have a match halfway around the ndigit-wide circular buffer. */
unsigned int
sum(unsigned int ndigits, struct posnod **nods)
{
	unsigned int i, result;

	result = 0;
	
	for (i = 0; i < NUM_POSNODS; i++) {
		struct posnod *nod;

		for (nod = nods[i]; nod != NULL; nod = nod->prev) {
			result += matches(nod, i, ndigits);
		}
	}

	return result;
}

/* calc2 calculates part 2. */
unsigned int
calc2(int fd)
{
	unsigned int result;
	unsigned int ndigits;
	struct posnod **posnods;
	
	result = 0;
	posnods = drain(fd, &ndigits);
	if (posnods == NULL) goto cleanup;

	result = sum(ndigits, posnods);
       
cleanup:
	posnods_free(posnods);
	return result;
}

#endif /* PART2 defined */

/*
 * Common code
 */

/* prnum prints an unsigned number in base 10 to stdout. */
void
prnum(unsigned int n)
{
	char buf[20];  /* max digits in a 64-bit num, plus newline  */
	char *bufp;
	unsigned int ndigits;
	unsigned int t;

	bufp = &buf[19];
	*bufp = '\n';

	if (n == 0) {
		write(0, "0\n", 2);
		return;
	}
	
	t = n;
	ndigits = 0;
	while (t != 0) {
		bufp--;
		*bufp = '0' + (t % 10);
		t /= 10;
		ndigits++;
	}

	write(0, bufp, ndigits + 1 /* newline */);
}

int
main(int argc, char **argv)
{
	int fd = -1;
	unsigned int r;
	
	if (argc == 1) {
		fd = 1; /* stdin */
	} else if (argc == 2) {
		fd = open(argv[1], O_RDONLY); /* text file */
	} else {
		write(0, "too many arguments\n", 19);
		return 1;
	}

	if (fd < 0)
	{
		write(0, "bad input\n", 10);
		return 1;
	}

#ifdef PART2
	r = calc2(fd);
	write(0, "P2 ", 4);
#else
	r = calc1(fd);
	write(0, "P1 ", 4);
#endif
	
	prnum(r);

	if (1 < fd) close(fd);
	return 0;
}
