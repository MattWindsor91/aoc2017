CC ?= clang
CFLAGS += --std=c89 --ansi --pedantic -Wall -Werror -Wextra

all: 1a 1b

1a: 1.c
	${CC} ${CFLAGS} 1.c -o 1a

1b: 1.c
	${CC} ${CFLAGS} -DPART2 1.c -o 1b
