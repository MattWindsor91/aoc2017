require 'set'

def redist(list, from, amt)
  length = list.length
  i = from
  until amt.zero?
    i = (i + 1) % length
    list[i] += 1
    amt -= 1
  end
end

def find_and_redist(list)
  amt, from = list.each_with_index.max_by { |x, _| x }
  list[from] = 0
  redist(list, from, amt)
end

def calc(initial)
  list = Array.new(initial)
  seen = {}

  cycles = 0
  until seen.member?(list)
    seen[list] = cycles
    find_and_redist(list)
    cycles += 1
  end

  [cycles, cycles - seen[list]]
end

list = ARGF.read.split.collect(&:to_i)
cycles, loopsize = calc(list)
print('Part 1: ', cycles, "\nPart 2: ", loopsize, "\n")
