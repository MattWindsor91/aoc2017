
# Calculates the number of moves needed to escape `maze`.
# Expects a block that takes the offset at a square and returns the
# amount (can be negative) to add to it before moving.
def moves_to_escape(maze)
  moves = pc = 0
  ary = Array.new(maze)
  max = maze.length - 1

  while pc.between?(0, max)
    val = ary[pc]
    ary[pc] += yield(val)

    pc += val
    moves += 1
  end

  moves
end

lines = ARGF.readlines
maze = lines.collect(&:to_i)
print 'Part 1: ', moves_to_escape(maze) { |_| 1 }, "\n"
print 'Part 2: ', moves_to_escape(maze) { |n| n < 3 ? 1 : -1 }, "\n"
