#lang racket

;
; Common
;

(define (check-passphrase normaliser phrase)
  (let ([words (map normaliser (string-split phrase))])
    (= (set-count (list->set words))
       (length words))))

(define (count-valid-passphrases normaliser port)
  (sequence-count (lambda (phrase) (check-passphrase normaliser phrase))
                  (in-lines port)))

;
; Part 1
;

(define (normalise-1 word) word)

(check-passphrase normalise-1 "aa bb cc dd ee")
(check-passphrase normalise-1 "aa bb cc dd aa")
(check-passphrase normalise-1 "aa bb cc dd aaa")

(define (count-valid-passphrases-1 port)
  (count-valid-passphrases normalise-1 port))

(call-with-input-file "4.in" count-valid-passphrases-1)

;
; Part 2
;

(define (normalise-2 word)
  (sort (sequence->list (in-string word)) char<?))

(check-passphrase normalise-2 "abcde fghij")
(check-passphrase normalise-2 "abcde xyz ecdab")
(check-passphrase normalise-2 "a ab abc abd abf abj")
(check-passphrase normalise-2 "iiii oiii ooii oooi oooo")
(check-passphrase normalise-2 "oiii ioii iioi iiio")

(define (count-valid-passphrases-2 port)
  (count-valid-passphrases normalise-2 port))

(call-with-input-file "4.in" count-valid-passphrases-2)